$(function () {
//изменение режима показа продуктов
    $('.change-mode-wr').click(function () {
        var count_of_modes = $(this).find('[data-mode]').length,
            active_mode_index = $(this).find('[data-mode].active').index(),
            next_active_mode_index = (active_mode_index + 1) >= count_of_modes ? 0 : active_mode_index + 1;

        $(this).find('[data-mode].active').removeClass('active');
        $(this).find('[data-mode]:eq(' + next_active_mode_index + ')').addClass('active');
        var active_mode = $(this).find('[data-mode].active').data('mode');
        $('.products-mesh').removeClass('one-col two-cols table').addClass(active_mode);
    });
});