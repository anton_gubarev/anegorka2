<?php
set_include_path('../pages/_partials');
defined('ROOT_PATH') || define('ROOT_PATH', realpath(__DIR__));
defined('PUBLIC_PATH') || define('PUBLIC_PATH', ROOT_PATH . '/public');

$uri = $_SERVER['REQUEST_URI'];
$uri = $uri == '/' ? '/index' : $uri;
$uri = substr($uri, 1, strlen($uri));
$layout = file_get_contents('../layout.phtml');

$page_path = realpath('../pages/' . $uri . '.phtml');
$_GLOBAL['page_uri'] = $uri;
if (file_exists($page_path)) {
    $page = file_get_contents($page_path);
} else {
    $page = file_get_contents('../pages/404.phtml');
}
$layout = str_replace('{{content}}', $page, $layout);
file_put_contents('../1', $layout);
include '../1';