/**
 * Определяем ширину для каждого выпадающего списка
 */
function calcWidthDropdownLists() {
    var VALUE_PADDING_RIGHT = 26; //отступ справа для стрелки
    var VALUE_PADDING_RIGHT2 = 20; //отступ справа для стрелки
    var overall_width = 0;
    $('.dropdown-list').each(function () {
        var width = 0;
        $(this).css({width: 'auto', minWidth: 'initial'});
        $(this).find('.options').css({position: 'relative'});
        $(this).find('.options li').each(function () {
            width = $(this).outerWidth(true) > width ? $(this).outerWidth() : width
        });
        $(this).find('.options').css({position: 'absolute'});

        var value_width = $(this).find('.value').outerWidth(true);
        width = value_width > width ? value_width : width;
        width += VALUE_PADDING_RIGHT;
        $(this).width(width).css({minWidth: width});
        $(this).data('width', width);
    });
    $('.dropdown-list').each(function () {
        overall_width += $(this).outerWidth(true);
    });

    var filter_block_width = $('.filter-block').width();

    if (filter_block_width > overall_width) {
        var increase_px = (filter_block_width - overall_width) / $('.dropdown-list').length - 20;

        $('.dropdown-list').each(function () {
            $(this).width($(this).data('width') + increase_px);
        });
    }
}

$(function () {
    $('body').click(function () {
        $('.dropdown-list').removeClass('opened');
    });
    $('.dropdown-list .options li').click(function () {
        var $dl = $(this).parents('.dropdown-list'),
            value = $(this).text();
        $dl.find('.value').text(value);
        $dl.removeClass('opened');
        return false;
    });

    $('.dropdown-list .value').click(function () {
        var $dl = $(this).parents('.dropdown-list');
        $('.dropdown-list').not($dl).removeClass('opened');
        $dl.toggleClass('opened');
        return false;
    });

    $('.mobile-filter .switch-filter-btn').click(function () {
        $('.category-filters').slideToggle('fast');
        $(this).toggleClass('opened');
    });
    calcWidthDropdownLists();

    $(window).resize(function () {
        calcWidthDropdownLists()
    });
});

/**
 * Выбор цвета
 */
function checkShowExpandBtn() {
    var colors_width = 0, btn_scale = 0;
    $('.colors-block .colors li').each(function () {
        colors_width += $(this).outerWidth(true);
    });
    if ($('.colors-block').width() > colors_width) {
        btn_scale = 0;
    } else {
        btn_scale = 1;
    }
    $('.colors-block .expand-btn').css({transform: 'scale(' + btn_scale + ')'});
}

$(function () {
    $('.colors li').click(function () {
        $(this).toggleClass('selected');
    });
    $('.colors-block .expand-btn').click(function () {
        $(this).parents('.colors-block').removeClass('compact');
        return false;
    });
    checkShowExpandBtn();
    $(window).resize(function () {
        checkShowExpandBtn()
    });
});