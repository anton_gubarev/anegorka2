$(function () {
    $('.products-table .product .minus').click(function () {
        var $product = $(this).parents('.product'),
            $amount = $product.find('.amount'),
            amount = parseInt($amount.val());
        if (amount <= 1) {
            $product.slideUp('fast');
        } else {
            $amount.val(amount - 1);
        }
    });

    $('.products-table .product .plus').click(function () {
        var $product = $(this).parents('.product'),
            $amount = $product.find('.amount'),
            amount = parseInt($amount.val());
        $amount.val(amount + 1);
    });

    $('.products-table .product .remove').click(function () {
        var $product = $(this).parents('.product');
        $product.slideUp('fast', function(){
            $product.remove();
        });
        return false;
    });
});