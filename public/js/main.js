function l(msg) {
    console.log(msg)
}

$(function () {
    //при скролле окна меняем сайдбар
    $(document).scroll(function () {
        var $aside = $('body > aside');
        if ($(document).scrollTop() === 0) {
            $aside.removeClass('scrolled');
        } else {
            $aside.addClass('scrolled');
        }
    });

    //открыть сайдбар
    $('body > aside .open-menu-icon').click(function () {
        $('body > aside').addClass('opened');
    });
    //скрыть сайдбар и мобильное меню
    $('body > aside .close-menu-icon').click(function () {
        $('body > aside').removeClass('opened');
        $('body').removeClass('mobile-menu-opened');
    });

    //открыть мобильное меню
    $('.open-mobile-menu').click(function () {
        $('body').addClass('mobile-menu-opened');
    });

    //показать попап для поиска
    $('.show-search-popup').click(function () {
        $('.search-popup').addClass('opened');
        return false;
    });

    $('.search-popup .popup-close-btn').click(function(){
        $('.search-popup').removeClass('opened');
    });
});