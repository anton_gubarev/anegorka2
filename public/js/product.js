function onResize() {
    var $body = $('body'),
        narrow_tablet_widht = 1023,
        narrow_tablet_class = 'tablet768',
        mobile_widht = 767,
        mobile_class = 'mobile320',
        $variants = $('.variants');

    if ($body.hasClass(narrow_tablet_class) && $(document).width() > narrow_tablet_widht) {
        $body.removeClass(narrow_tablet_class, mobile_class);
        $('.narrow-prefs-wr .prefs').remove().appendTo($('.wide-prefs-wr'));
        $('.mobile-name-wr .name-wr').remove().appendTo($('.wide-name-wr'));
        $('.narrow-variants-wr .variants-wr').remove().appendTo($('.wide-variants-wr'));
        $variants.slick('unslick');
        makeVariantsCarousel(4);
    } else if (!$body.hasClass(narrow_tablet_class) && $(document).width() <= narrow_tablet_widht && $(document).width() > mobile_widht) {
        $body.addClass(narrow_tablet_class).removeClass(mobile_class);
        $('.wide-prefs-wr .prefs').remove().appendTo($('.narrow-prefs-wr'));
        $('.mobile-name-wr .name-wr').remove().appendTo($('.wide-name-wr'));
        $('.wide-variants-wr .variants-wr').remove().appendTo($('.narrow-variants-wr'));
        $variants.slick('unslick');
        makeVariantsCarousel(5);
    } else if (!$body.hasClass(mobile_class) && $(document).width() <= mobile_widht) {
        $('.wide-name-wr .name-wr').remove().appendTo($('.mobile-name-wr'));
        $('.wide-prefs-wr .prefs').remove().appendTo($('.narrow-prefs-wr'));
        $('.wide-variants-wr .variants-wr').remove().appendTo($('.narrow-variants-wr'));
        $variants.slick('unslick');
        makeVariantsCarousel(5);
    }
}

function makeVariantsCarousel(slides) {
    if ($('.product-page .variants').length > 0) {
        $('.product-page .variants').slick({
            infinite: true,
            slidesToShow: slides,
            slidesToScroll: slides,
            variableWidth: true
        });
    }
}


$(function () {
    makeVariantsCarousel(4);
    onResize();
    $(window).resize(function () {
        onResize()
    });

    $('.popup-close-btn').click(function () {
        window.history.back()
    });

    //Добавить в корзину
    $('.add-to-cart-btn').click(function () {
        if (!$(this).hasClass('added')) {
            $(this).addClass('added btn-dark').text('Перейти в корзину');
            return false;
        }
    });

    //развернуть все параметры
    $('.all-params-link').click(function () {
        $('.params li.hidden').css({display: 'flex'});
        $(this).hide();
        return false;
    });

    //открыть попап с таблицей размеров
    $('.sizes-table-link').click(function () {
        $('.sizes-table-popup').fadeIn('fast');
        $('body').addClass('showed-table-popup');
        $(document).scrollTop(0);
        return false;
    });
    //закрыть попап с таблицей размеров
    $('.sizes-table-popup .close').click(function () {
        $('.sizes-table-popup').fadeOut('fast');
        $('body').removeClass('showed-table-popup');
    });
});